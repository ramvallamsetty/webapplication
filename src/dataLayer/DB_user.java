package dataLayer;

import sun.security.util.Password;

import java.sql.*;

public class DB_user {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/webApplication";

    static final String USER = "webappuser";
    static final String PASSWRD = "P@ssw0rd";

    public boolean isValidUserLogin(String sUserName, String sUserPassword){

        boolean isValidUser = false;

        Connection conn = null;
        Statement stmt = null;
        String sql = "";
        try{
            //Register JDBC driver
            Class.forName(JDBC_DRIVER);

            //Open the connection
            conn = DriverManager.getConnection(DB_URL, USER, PASSWRD);

            stmt = conn.createStatement();

            sql =  "SELECT * FROM users WHERE user_name = \"" +
                    sUserName + "\" AND user_password = \"" + sUserPassword + "\"";

            System.out.println(sql);

            ResultSet rs = stmt.executeQuery(sql);

            if(rs.next()){
                isValidUser = true;
            }

            rs.close();
            stmt.close();
            conn.close();

        } catch(SQLException se){
            se.printStackTrace();

        }catch(Exception e){
            e.printStackTrace();

        } finally{
            try{
                if(stmt != null)
                    stmt.close();
            }catch(SQLException s){
                s.printStackTrace();
            }
            try{
                if(conn != null)
                    conn.close();
            }catch(SQLException s){
                s.printStackTrace();
            }
        }
        System.out.println("Closing DB Connection -- GoodBye");

        return isValidUser;
    }
}
